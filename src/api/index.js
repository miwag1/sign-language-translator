const apiKey = process.env.REACT_APP_API_KEY

/* This is used for when create user to API*/ 
export const createHeaders = () => {
    return{
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    }
}
