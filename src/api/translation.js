//import { resolvePath } from "react-router-dom"
import { createHeaders } from "."

const apiUrl = process.env.REACT_APP_API_URL
//POST create new item
//PATCH Update parts of the record
//GET Read records
//DELETE 

//Async video 13 12.00
export const translationAdd = async (user, translation) => {

 try{
    const response = await fetch( `${apiUrl}/${user.id}`,{
        method: 'PATCH', //uppdating existing record/ adding new record
        headers: createHeaders(),
        body: JSON.stringify({
            //username: user.username,
            translations: [...user.translations, translation]

        })
    })
    if(!response.ok) {
        throw new Error('Could not update the translation')
    }

    const result = await response.json()
    return [null, result]

 }
 catch (error){
    return [error.message, null] // error message null for the trnslation
 }
}

export const translationClearHistory = async(userId)=> {
    try {
        const response = await fetch(`${apiUrl}/${userId}`,{
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations:[] //reset data
            })
        })
        if(!response.ok){
            throw new Error('Could not update translations')
        }
       const result = await response.json()
       return [null, result]
    } catch (error) {
        return [error.message, null]
    }

}
//(8:43)