import { createHeaders } from "./index"

const apiUrl = process.env.REACT_APP_API_URL

/*This is not adding to the database reading data from the database load from the api */ 
export const checkForUser = async(username) => {
  try{
      const response = await fetch (`${apiUrl}?username=${username}`)// https://mi-experis-api.herokuapp.com/translations?username=dewaldels
      if(!response.ok){
       throw new Error('Could not complete request.')
  }
   const data = await response.json()
   return [null,data]
}

  catch (error) {
    return [error.message, null]
  }

}

/*This needs authentication because create user to the database*/ 
export const createUser = async(username) =>{
    try{
        const response = await fetch (apiUrl, {
            method: 'POST', // Create resource
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations:[]
            })
    })
        if(!response.ok){
         throw new Error('Could not create user with username.'+ username)
    }
     const data = await response.json()
     return [null,data]
  }
  
    catch (error) {
      return [error.message, []]
    }

}



export const loginUser = async (username) => {

    const [checkError, user] = await checkForUser(username)



    if (checkError !== null) {

        return [checkError, null]

    }

    // if there is user 
    if(user.length >0){
        return [null, user.pop()]
    }
 
//const [createError, newUser]= createUser(username)
return await createUser(username)
}

export const userById = async (userId) =>{
     try{
       const response = await fetch (`${apiUrl}/${userId}`)
       if (!response.ok){
        throw new Error ('Could not fetch user')
       }
       const user = await response.json()
       return [null, user]
      } 
     catch (error) {
      return [error.message, null] //null for the user

     }

}