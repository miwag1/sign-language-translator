import { useEffect } from "react"
import { userById } from "../api/user"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import { STORAGE_KEY_USER } from "../const/StrageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/strage"

const Profile =() => {
    const {user, setUser} = useUser()

    // profile.jsx is watching  withAuth  
/*     const logout = ()=> {

    }
 */
    //This is to update adding translation withhout logout and login again//video 14 improve it 

         useEffect(() => {
        const findUser = async () => {

            const [error, latestUser] =await userById(user.id)
           if(error === null){
             storageSave(STORAGE_KEY_USER, latestUser)
             setUser(latestUser)
           }
        }
        //findUser()

    },[setUser, user.id]) 


    return (
     <> 
        <h1 style={{color:"grey"}}>Profile</h1>
         {/* <ProfileHeader username = {user.username}/>  */}
         <ProfileActions />
         <div className="content_3">
         
         <ProfileTranslationHistory translations={user.translations}/> 
         
         </div>
     </>
    )

}
export default withAuth(Profile)