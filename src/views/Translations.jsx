import { translationAdd } from "../api/translation"
import TranslationsForm from "../components/Translations/TranslationsForm"
import { STORAGE_KEY_USER } from "../const/StrageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/strage"




const Translations =() => {
   // const [text, setText] = useState("")
    const {user, setUser} = useUser ()

/*     const handleSignClicked = (signId) => {
      setSign (SIGNS.find(sign => sign.id === signId))
    } */

/*     const handleSignClicked = (e) => {
        setText ([e.target.value]);
    }

    const list = text.map((text)=> <li key ={text}> {text}</li>); */




    // video 13 06.58
    const handleTranslatedClicked =  async (notes) => { //The Sign language characters must appear in the “translated” box.
        console.log(notes)

        const translation = notes.trim()

        console.log(translation);


        // Send an HTTP Request

        const [error, updatedUser] =await translationAdd(user, translation)
/*        const [ error, updatedUser] = await translationAdd(user, translation)
        if(error !== null){ // if there is somthing error
           //Bad
            return
        }*/
        // Keep UI state and Server state in sync
        storageSave(STORAGE_KEY_USER, updatedUser)
        //Update context state
        setUser(updatedUser)
  

        console.log('Error', error)
        //console.log('UpdatedUser', updatedUser)
        console.log('Result', updatedUser)

    }


    return (
        <>
        <div className="wrapper_t">
        <section id = "translation-notes">
        {/* onTranslation id from translaters.jsx */}
            <TranslationsForm onTranslation ={handleTranslatedClicked}/> 
        </section>
        </div>
        </>
    )

}
export default withAuth(Translations)