import LoginForm from '../components/Login/LoginForm'
import '../App.css'

const Login =() => {

    
    return (
        <>
        <div className='login'>
            <div className='login_div'>
                <img src="img/Logo-Hello.png" alt='' style={{width: 150,
        height: 150, }}></img>
            </div>
            <div className=''>
            <div className='heading' style={{color: "white", fontSize: "45px", padding:"2px"}} >Lost in Translation</div>
            <div style={{color: "white", fontSize: "35px", padding:"10px"}} >Get started</div>
            </div>
        <LoginForm />
        </div>
        </>

    )

}
export default Login