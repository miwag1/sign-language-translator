import {createContext, useContext, useState} from "react"
import { STORAGE_KEY_USER } from "../const/StrageKeys"
import { storageRead } from "../utils/strage"
//Context -> exposing
//Provider ->managing state
const UserContext= createContext()

//Hook return userContext -> User context privide state
export const useUser =() => {
    return useContext(UserContext) // {user.setUser}
}

//Provider ->managing statoe
const UserProvider = ({children}) => {
    // magic strings /number
    const [user, setUser] = useState( storageRead(STORAGE_KEY_USER))//null->storageRead() redirect prifilepage

    const state ={
        user,
        setUser
    }

    return (
        <UserContext.Provider value ={state}>
            {children}
        </UserContext.Provider>

    )

}


export default UserProvider