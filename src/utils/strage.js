//localstrage is not in sync the server data
//If Heroku shatdown the data is reset
//Use sessionStrage when we close the tab data is still stored

const validateKey = key => {

    if(!key|| typeof key!== 'string'){
        throw new Error ('strageSave: Invalid strage key provided')
    }
}


export const storageSave =(key, value) => {
    
    validateKey(key)

    
    if(!value){
        throw new Error('storageSave: No value for '+ key) 
    }
    sessionStorage.setItem(key, JSON.stringify(value))
}

export const storageRead = key => {
    
    validateKey(key)

    const data = sessionStorage.getItem(key)

    if(data) {
        return JSON.parse(data)
    }

    return null
}

export const starageDelete = key => {
    
    validateKey(key)

    sessionStorage.removeItem(key)
}