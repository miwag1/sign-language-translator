import {useForm} from 'react-hook-form';
import { loginUser} from '../../api/user'
import {useState, useEffect} from 'react';
import { storageSave } from '../../utils/strage';
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext';
import { STORAGE_KEY_USER } from '../../const/StrageKeys';

const usernameConfig ={
    required:true,
    minLength:3
}

const LoginForm = () =>{
    const {register,handleSubmit,formState: {errors}} =useForm();
    const {user, setUser} =useUser() // If user chenges UseEffect needs to run
    const navigate = useNavigate()

    //Local state
    const [loading, setLoading] = useState(false);
    const [apiError, setApiError] = useState(null)

    // Side Effect -> if user changed it should run once
  useEffect (()=> {
    if (user !== null){
        navigate('translations') // move to "Translations"
    }
     //console.log("User has changed!", user)
  },[user, navigate]) // Empty Deps -Only run 1ce

    // Event Handlers
    const onSubmit =async({username}) =>{
        setLoading(true);
        const [error, userResponse] =await loginUser(username)

        // If you successfully log in it is going to store in the strage
        if(error!==null){
            setApiError(error)
        }
        if(userResponse!==null) {
          storageSave(STORAGE_KEY_USER, userResponse)
          setUser(userResponse)
        }
         setLoading(false);
    };

    console.log(errors)

    //Render Functions
    const errorMessage =  (() =>{
        if (!errors.username){
            return null
        }
        if (errors.username.type ==='required'){
           return <p>Username is required</p>
        }

        if (errors.username.type ==='minLength'){
            return <p>Username is too short (min. 3)</p>
        }

    }) ()



    return(
    <>

{/* <div class="center_box"> */}

<div className="wrapper">
        <div className='box'>
          <div className=''>
    <form onSubmit ={handleSubmit(onSubmit)}  className="search-form">
        
            <label htmlFor="username">
            <input style={{fontSize: "24px"}} type="text" 
                   placeholder='What´s your name'
                   {...register("username", usernameConfig)}
                   />
            </label>
                   
        <button name="loginButton" type="submit" style={{fontSize: "17px", color:"white",backgroundimage:"none"}} disabled={loading}>Login</button>

    </form>

    </div>

    <div className='content_message'> 
            {errorMessage}
            {loading && <p>Loading in...</p>}
            {apiError && <p>{apiError}</p>}

    </div>
    
    {/* <div className='box-title'></div> */}
    </div>
    

    
    </div>

    </>
    )
}

export default LoginForm