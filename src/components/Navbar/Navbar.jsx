import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import ProfileHeader from "../Profile/ProfileHeader"


const Navbar = () =>{

    //Use useUser Hook to hide NavLink to Translation and profile in Login page with If
    //const {user} = useUser()
    const {user} = useUser()
 
    return (
        
           <div className="navbar" >

                <img src="img/Logo.png" alt="" style={{height: "20", width: "20"}}></img>
                <h3 className="heading" style={{color: "white"}} >Lost in Translations</h3>
            
            
            
            { user!==null &&
                <nav >
                    <ul className="">
                       
                        <li>
                        <NavLink to="/translations">Translations</NavLink>
                        </li>
                        <li>
                        <NavLink to ="/profile">Profile</NavLink>
                        </li>
                        <li>
                          < ProfileHeader username = {user.username} /> 
                        </li>
                        </ul>
                </nav>
            }
           
            </div>
            
        
    )
}
export default Navbar