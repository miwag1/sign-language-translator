import { Link } from "react-router-dom"
import { translationClearHistory } from "../../api/translation"
import { STORAGE_KEY_USER } from "../../const/StrageKeys"
import { useUser } from "../../context/UserContext"
import { starageDelete, storageSave } from "../../utils/strage"



// logout should be in component/-> it is not send to the profile any more
//the function logout move here from profile.jsx
const ProfileActions= ()=>{ 

    const {user, setUser} = useUser() // we dont need user here just use setUser

    //invoking the function in the parents (profile.jsx)//8:22 v10
    const handleLogoutClick = () =>{
     if(window.confirm("Are you sure?")){
        //Send an event to the parent.
        //Thease are moved here from profile.jsx
        //It is better to make Strageremove function(remove user) insted of strage save null user
        //storageSave(STORAGE_KEY_USER, null)// Value user does not existing any more
        starageDelete(STORAGE_KEY_USER)
        setUser(null)
    }   
    }

    const handleClearHistoryClick = async() => {
       if(!window.confirm('Are you sure?\nThis can not be undone!')){
        return
       }
       const [clearError] = await translationClearHistory(user.id)
       if (clearError !== null){

        return
       }
       const updatedUser={
        ...user,
        translations:[]

       }
       storageSave(STORAGE_KEY_USER, updatedUser)
       setUser(updatedUser)
    }
    return(
    
        <ul>
        {/* <li><Link to ="/translations">Translations</Link></li> */}
        <li><div onClick={handleClearHistoryClick} className="button_solid" style={{padding:"1em"}}><a href="#">Clear history</a></div></li>
        <li><div onClick={handleLogoutClick} className="button_solid" ><a href="#">Logout</a></div></li>
        </ul>
    )
}

export default ProfileActions