import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory= ({translations})=>{
    // display the last 10 translations for the current user
    const arr_length = translations.length;
    const translationList = translations.slice(arr_length-10,arr_length).map(
        (translation, index)=> <ProfileTranslationHistoryItem key={index +'-'+ translation} translation ={translation}/>)
    return(
        <section>
            <div className="box-title"> Your translation history</div>

            {translationList.length === 0 && <p>You have no translations yet.</p>}

            <ul className="box-content">
             { translationList }
            </ul>
            </section>

    )
}

export default ProfileTranslationHistory