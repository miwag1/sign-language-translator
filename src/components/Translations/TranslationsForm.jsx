import { useForm } from "react-hook-form";
import { useState } from "react";
import { Scrollbars } from 'react-custom-scrollbars-2';



const TranslationsForm = ({onTranslation}) => {

    const { register, handleSubmit} = useForm()
    const [tranlateSign, setTranlateSign ] = useState([]); //all signs in this Array


    const onSubmit = ({translationNotes}) => {
        console.log(translationNotes)
        onTranslation(translationNotes) 

       // const img_url= "img/";
        //const arr= [ "img/a.png","img/b.png", "img/c.png"]
       // const text= "Hello";// Input
        const chars = translationNotes.toLowerCase().split('');
        const images= (!(translationNotes.toLowerCase().match(/[a-z]/)))?  // If the text is not alphabet
                  <p>You need to write alphabet</p>          // show message
                 : chars.map((char)=> {                    // otherwise show the sign
            return <img src ={`img/${char}.png`} alt=''></img>
        }
      )
        
      setTranlateSign(images)
    }

    return (
      
        <div className="translation">
        <div className='box_t'>
          
        <form onSubmit = {handleSubmit(onSubmit)} className='search-form'> 


                <input style={{fontSize: "24px"}} type ="text" {...register('translationNotes')} placeholder="Input Text to Translate" />
                <button type = "submit" name="translation-button"></button>
 
            
            
        </form>
        
         <div className="content_2">  
        
        <Scrollbars style={{ width: 800, height: 150}}>{tranlateSign}</Scrollbars>
        
        
         </div> 
        
        </div>
        </div>
        
    )

}
export default TranslationsForm