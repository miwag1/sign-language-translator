import { Navigate } from "react-router-dom"
import { useUser } from "../context/UserContext"

//Hight order component
const withAuth= Component => props => {
    const {user} =useUser()
    if(user !== null){ // User is Login go to profile
        return <Component {...props}/>
    } else {
        return <Navigate to="/" /> //user is null means user does not Login anymore (Move to Login page)
    }
}

export default withAuth