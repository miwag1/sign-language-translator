## Assigment 2 -Create a Sign Language Translator using React-

Build an online sign language translator as a Single Page Application using the React framework. 
-requirement-
• React framework
• React Router to navigate between pages
• API to store the users and their translations

## Deacription

This app has 3 sections to show the functions to login app or create new login name, input word tp tranlate, check the history of the serching  and deleat them and logout from app. 

```
1) The Startup Page
2) The Translation Page
3) The Profile Page
```

## Startup/Login Page

-  The user must be able to enter their name and Save the username to the Translation API
-  When you log in with the saved name, the app must display the main page, the translation page.

***
I used sessionStrage to manage the session because these info can be accessed without password is not sensitiv information 
***


## Translation Page

- A user may only view this page if they are currently logged into the app. 
- Translation-Button: The user types in the input box at the top of the page and click the button to the right of the input box to translate the word to sign language.Tha sign language will be displayed the translation-box 
- Translations must be stored using the API.

***
ONLY the text must be stored
***

## Profile page

- The profile page must display the last 10 translations for the current user and they are only text.
- Clear history-Button: This should “delete” in your API and no longer display on the profile 
page. 

***
Delete the records, but you should NEVER delete data from a database.
***
- Logout-Button: This should clear all the storage 

